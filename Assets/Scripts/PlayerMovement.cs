﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

    // set a flag for click / tap .... if true the user has clicked or tapped 
    // on a square to move to. False indicates lemm has finished moving.
    private bool shouldMove = false;
    // destination point
    private Vector3 destination;
    // vertical position of player (constant, should be 0)
    private float yPos;

    // saves players original location (if they die)
    private Vector3 origin;

    // gameController script
    private GameObject gameController;

    /*     PUBLIC attributes      */

    // public scalar which changes duration of movement (not speed)
    public float duration = 35.0f;
    // scalar responsible for rotation speed (greater # = faster rotation)
    public float rotationSpeed = 10.0f;


    /*      UNITY functions      */

    // Use this for initialization
	void Start () {
	    // save the y axis value 
        yPos = this.transform.position.y;
        origin = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	    // check if screen is touched / clicked
        if ((Input.touchCount > 0 && Input.GetTouch(0).phase == 
             TouchPhase.Began) || (Input.GetMouseButtonDown(0))) {

            // create a RaycastHit struct
            RaycastHit hit;
            Ray destRay;
            // for unity editor or unity standalone
            #if (UNITY_EDITOR || UNITY_STANDALONE)
            destRay = Camera.main.ScreenPointToRay(Input.mousePosition);

            #elif (UNITY_ANDROID || UNITY_IPHONE || UNITY_WP8)
            destRay = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            #endif

            if (Physics.Raycast(destRay, out hit)) {
                // sets flag to indicate movement
                shouldMove = true;
                // save the clicked or tapped position
                destination = hit.point;

                // don't change the y-axis position
                destination.y = this.transform.position.y;

                // save the origin, but not if you're already dead
                if (this.transform.position.y > .7)
                    origin = this.transform.position;

                // save the target direction for rotation
                Vector3 targetDir = hit.point - transform.position;
                // rotate the player
                rotatePlayer(targetDir);
            }
        }
        // in charge of player movement
        // checks if shouldMove flag for movement is true and current player 
        // position is not the same as the clicked or tapped position
        if (shouldMove && !Mathf.Approximately(this.transform.position.magnitude,
            destination.magnitude))
        {
            // move the player to the clicked or touched position
            // greater the DURATION, less the SPEED
            this.transform.position = Vector3.Lerp(this.transform.position, 
                destination, 1 / (duration * 
                (Vector3.Distance(this.transform.position, destination))));

        }
        // set shouldMove to false if it has reached the destination (is equal)
        else if (shouldMove && Mathf.Approximately
                 (this.transform.position.magnitude, destination.magnitude)) {
            shouldMove = false;
            Debug.Log("Destination Reached");
        }
        // player death
        if (this.transform.position.y < .5)
        {
            shouldMove = false;
            this.transform.position = origin;
            this.transform.rotation = Quaternion.AngleAxis(0, Vector3.up);
            gameController = GameObject.FindGameObjectWithTag("GameController");
            PlayerLifeMechanics lifeScript = gameController.GetComponent<PlayerLifeMechanics>();
            lifeScript.lives -= 1;
        }
	}
    // controls player rotation
    void rotatePlayer(Vector3 targetDir) {
        // calculates the Arc Tangent along z and x axes, convert to degrees
        float angle = Mathf.Atan2(targetDir.x, targetDir.z) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }
}
