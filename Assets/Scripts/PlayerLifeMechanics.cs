﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerLifeMechanics : MonoBehaviour {

    public int lives = 20;
    // sets amount of juice originally had
    public float juice = 50;
    // prefab _Lemm
    public GameObject player;
    // prefab light
    public Light light;
    // explosion, borrowed from a unity tutorial on 2d space shooters
    public GameObject deathExplosion;

    // GUI elements
    public Text juiceText;
    public Text lifeText;
    //public Text fallAwayTimer;
   
    // private objects
    private GameObject playerInstance;
    private GameObject explosion;
    private Text juiceTextBox;
    private Text lifeTextBox;
    private Text timerTextBox;

    // headlamp
    private GameObject headlamp;
    private Light lightInstance;

    // timer for headlamp
    private float juiceTimer;
    private bool spaceToggle = false;

    // sounds for headlamp
    private AudioSource turnOn;
    private AudioSource turnOff;

    void Awake() {
        // get the player instance
        playerInstance = GameObject.FindGameObjectWithTag("Player");

        turnOn = GameObject.FindGameObjectWithTag("GameController").GetComponent<AudioSource>();
        turnOff = playerInstance.GetComponent<AudioSource>();

        // get headlamp
        headlamp = GameObject.FindGameObjectWithTag("Light");
        lightInstance = headlamp.GetComponent<Light>();
        juiceTimer = 0;
        juiceTextBox = Instantiate(juiceText) as Text;
        lifeTextBox = Instantiate(lifeText) as Text;
        //timerTextBox = Instantiate(fallAwayTimer) as Text;
        lifeText.text = "Life Remaining: " + lives;
        juiceText.text = "Juice: " + juice + "%";
    }

    void FixedUpdate() {
        // controls juice usage
        if (spaceToggle)
        {
            juice -= Time.deltaTime * 5f;
            juiceText.fontSize = 30;
        }
        if (juice < 0)
            juice = 0;


    }
    // controls juice usage
    void Update() {
        // tests for juice variable positive
        if (juice <= 0)
        {
            lightInstance.intensity = 4;
            juice = 0;
            spaceToggle = false;
            Debug.Log("Juice amount: " + juice.ToString("##.##"));
        }
        juiceText.text = "Juice: " + juice.ToString("##") + "%";
        // needs update for mobile
        if (Input.GetKeyDown(KeyCode.Space))
        {
            spaceToggle = !spaceToggle;
            if (spaceToggle && juice != 0)
            {
                turnOn.Play();
                juice -= Time.deltaTime;
                juiceText.text = "Juice: " + juice.ToString("##") + "%";
                juiceText.fontSize = 18;
                lightInstance.intensity = 8;
            } else if (!spaceToggle)
            {
                turnOff.Play();
                juiceText.fontSize = 14;
                Debug.Log("Juice left = " + juice);
                lightInstance.intensity = 4;
                juiceTimer = 0;
                // juiceText.text = "Juice: " + ((int)juice).ToString("##") + "%";
            }
        }
        // UPDATE life remaining
        lifeText.text = "Life Remaining: " + lives;
        // PLAYER LOSE
        if (lives < 1)
        {
            lifeText.text = "Game Over!";
            juiceText.text = "Game Over!";
            Invoke ("ReloadMenu", 3.0f);
        }
        // PLAYER WINS
        if (playerInstance.transform.position.z > 49)
        {
            lifeText.text = "You Win!";
            juiceText.text = "You Win!";
            Invoke ("ReloadMenu", 3.0f);
        }
    }

    // get more juice from pickups
    void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.tag == "Juice")
        {
            juice += 10;
        }
    }

    void ReloadMenu() {
        Application.LoadLevel("_SceneSplash");
    }
}
