﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GroundController : MonoBehaviour {

    // Create a private container to store ground blocks
    protected List<GameObject> gbList;

    // create a private container to store procedural flags
    protected List<int> flags;
    
    // wrapper class groundCube is used to store the cubes for ground (as 
    // nodes) and also an int flag that is used during hole creation

    // a 1 x 1 x 1 cube used for the ground
    public GameObject groundCubePrefab;

    // the juice ground pickup item
    public GameObject juicePrefab;

    // size (z coord) of how tall the level is going to be
    public int levelSize = 20;

    // timer (max time) before the ground starts falling
    public float groundTimer = 30f;

    // ratio for difficulty, aka how many obstacles should there be
    public float difficulty = 10f;

    // falling ground surface
    public Material groundSurfaceFalling;

    // text box showing ground timer
    public Text fallAwayTimer;

    // divider in the inspector pane

    public bool ______________________;


    // these constants define where the first ground block goes
    private const int xPos = -9;
    private const int yPos = 0;
    private const int zPos = -5;
    
    // constant defines the maximum x coord bound for cubes
    private const int maxxPos = 9;
    
    // static integer to indicate the offset where the new cube should go
    protected static int xOffset = 0;
    // protected static int zOffset = 0;
   
    // shows text box warning for how long the row is going to fall
    private Text timerTextBox;

    // array for deletion
    private GameObject[] deleteMe;


    // Spawns initial ground cubes
    // obstacle is a value from 0 - 10+ which is the likelihood that there will
    // be an obstacle on that position
    void spawnGround(GameObject groundCube, int xOff, int zOff, int flag) {
        GameObject groundInstance;
        // Instantiates an instance of groundCube
        groundInstance = Instantiate(groundCubePrefab) as GameObject;
        int newYpos;
        // test flag value, if true this block will be a "hole"
        if (flag > 75)
        {
            newYpos = yPos - 40;
        } else
        {
            newYpos = yPos;
        }

        // move object to proper x coordinate using constant xPos and xOffset
        // move object to proper z position using constant zPos and zOffset
        Vector3 position = new Vector3 (xOff, newYpos, zOff);
        // add a juice thing if flags match up
        if (flag < 70 && flag > 67)
        {
            GameObject juiceInstance;
            juiceInstance = Instantiate(juicePrefab) as GameObject;
            juiceInstance.transform.position = new Vector3 (xOff, newYpos + 1, zOff);
        }
        groundInstance.transform.position = position;
        gbList.Add(groundInstance);
    }

    // known bug - gbList's first element points to the last block (upper right most)
    public GameObject[] removeLine() {

        // stores the list of objects that will begin to fall / be deleted
        GameObject[] fallingBlocks;
        // checks to see if the array has less than two complete lines
        if (gbList.Count < maxxPos - xPos + 2) {
            // get the rest of blocks form gbList
            fallingBlocks = new GameObject[gbList.Count];
            fallingBlocks = gbList.ToArray();
            // delete remainder of gbList
            gbList.Clear();
        } 
        // gbList has more than 13 objects, remove a line and save it to an array
        else {
            int oneRow = maxxPos - xPos + 1;
            float numRows = Random.value * 100;

            // procedural generation for falling rows
            if (numRows > 70)
                oneRow = oneRow * 3;
            else if (numRows < 30) 
                oneRow = oneRow * 2;

            fallingBlocks = new GameObject[oneRow];
            // gets the range from 0 - 13 and saves it to the array
            fallingBlocks = gbList.GetRange(0, oneRow).ToArray();
            // removes the 13 values.
            gbList.RemoveRange(0, oneRow);
            }
        return fallingBlocks;
    }

    /*       Unity Methods       */

    // Use this for initialization
    void Awake() {
        // initialize list of Game Objects
        gbList = new List<GameObject>();
        flags = new List<int>();
        int flagVal = 1;
        timerTextBox = Instantiate(fallAwayTimer) as Text;
        // this nested for loop counts through zPos to Level Size and
        // creates a line of cubes on every row. Inserts FLAGS for procedural
        // generation of obstacles in the next loop
        bool isJtrue = false;
        int saveJ = 0;
        for (int i = zPos; i <= levelSize; i++)
        {
            for (int j = xPos; j <= maxxPos; j++)
            {
                // create list of flags
                if (i > 2)  {
                    if (isJtrue && j == saveJ) {
                        flagVal = 81;
                        isJtrue = false;
                    }
                    // calculation makes it so that the i value (z axis) will 
                    // be added, increasing likelihood of holes
                    float rand = Random.value * difficulty + 0.1f*i;
                    flags.Add(flagVal += (int) rand);
                    Debug.Log(rand);
                    Debug.Log(flagVal);
                    if (flagVal > 100) {
                        flagVal = 20;
                        saveJ = j;
                        isJtrue = true;
                    }
                }

                // spawn ground call
                spawnGround(groundCubePrefab, j, i, flagVal);
            }
        } 
    }
    
    // Update is called once per frame
    void FixedUpdate () {


        bool isFirstFall = true;

        // prep a new row to fall
        if (groundTimer == 5)
        {
            deleteMe = removeLine();
            Renderer rend;
            Color colorEnd = Color.red;
            foreach (GameObject i in deleteMe)
            {
                rend = i.GetComponent<Renderer>();
                rend.material.color = colorEnd;
            }
        }
        // subtract the timer by deltaTime
        groundTimer -= Time.deltaTime;
        // show countdown timer
        fallAwayTimer.text = "Unstable Rows In: " + groundTimer.ToString("#.##");

        if (groundTimer < 0)
        {
            Collider coll;
            groundTimer = 5;
            // iterates through the blocks to be removed and turns gravity on
            foreach (GameObject i in deleteMe)
            {
                coll = i.GetComponent<Collider>();
                coll.attachedRigidbody.mass = (int)Random.value * 100;
                coll.attachedRigidbody.useGravity = true;
                coll.attachedRigidbody.isKinematic = false;
            }
        }
    }
    /*     End Unity Methods     */
}

/*
 * 
 * 
 * 
 * 
 * 
 * 
 */
