﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

    // player object
    public Transform target;
    // offset for camera movement
    public Vector3 offset;
    // Distance player can move in z axis before camera follow
    public float zMargin = 2f;  
    public float zSmooth = 10f;  // how smoothly camera catches up with Lemm
    public Vector3 maxZval;     // max z value camera can have
    public Vector3 minZval;     // min z value camera can have


    // Sets the offset so that the camera doesn't move downward
	void Start () {
        offset = transform.position - target.position;
	}

    // controls camera Z axis movement
    void FixedUpdate() {
        if (!target)
        {
            GameObject newPlayer = GameObject.FindGameObjectWithTag("Player");
            target = newPlayer.transform;
        }
        TrackPlayer();
    }

    bool checkZMargin() {
        return Mathf.Abs(transform.position.z - target.position.z) > zMargin;
    }

    void TrackPlayer() {
        // By default the target z position of camera is current z coordinate
        float targetZ = transform.position.z;

        if (checkZMargin())
        {
            targetZ = Mathf.Lerp(transform.position.z, target.position.z, 
                                 zSmooth * Time.deltaTime);
        }
        // don't move beyond maxZval or before minZval
        targetZ = Mathf.Clamp(targetZ, minZval.z, maxZval.z);

        // set camera to new position
        transform.position = new Vector3(transform.position.x, 
                                         transform.position.y, targetZ);
    }
}
