﻿using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour {

    private GameObject gameController;

    void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            // gets the juice variable and adds 5
            gameController = GameObject.FindGameObjectWithTag("GameController");
            PlayerLifeMechanics lifeScript = gameController.GetComponent<PlayerLifeMechanics>();
            lifeScript.juice += 5;
        }
    }
}
