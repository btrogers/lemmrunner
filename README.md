# README #

Lemm Runner 3D - Assignment 4

### What is this repository for? ###
* Brandon Rogers - Lemm Runner 3D
* CS 583 - SDSU - Professor Price

### How do I get set up? ###

* Please clone the files and set them up in the unity standalone environment
* You can also DOWNLOAD a .exe in downloads if you would like to play it directly.

### Controls ###

* Use mouse left click to move around
* Use spacebar to activate your headlamp (note, this consumes Juice, a precious resource!)
* Get to the end of the level to win